#include "line_buffer.h"
#include <stdio.h>

void line_buffer__init(line_buffer_s *buffer, void *memory, size_t size) {
  buffer->memory = memory;
  buffer->max_size = size;
  buffer->write_index = 0;
}

bool line_buffer__add_byte(line_buffer_s *buffer, char byte) {
  if (buffer->write_index < buffer->max_size) {
    ((char *)buffer->memory)[buffer->write_index++] = byte;
    return true;
  }
  return false;
}

bool line_buffer__remove_line(line_buffer_s *buffer, char *line, size_t line_max_size) {
  size_t line_length = 0;
  size_t start_index = 0;
  // Find the start index of the line
  for (size_t i = 0; i < buffer->write_index; i++) {
    if (((char *)buffer->memory)[i] == '\n') {
      line_length = i - start_index + 1;
      break;
    }
  }
  if (line_length == 0 && buffer->write_index == buffer->max_size) {
    // Buffer is full and no newline character found, treat as complete line
    line_length = buffer->write_index - start_index;
  }
  if (line_length > 0 && line_length <= line_max_size) {
    memcpy(line, &((char *)buffer->memory)[start_index], line_length);
    line[line_length - 1] = '\0'; // Null terminate the line
    printf("line: %s\n", line);
    memmove(buffer->memory, &((char *)buffer->memory)[start_index + line_length],
            buffer->write_index - start_index - line_length);
    buffer->write_index -= line_length;
    return true;
  }
  return false;
}