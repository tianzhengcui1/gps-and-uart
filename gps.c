// gps.c
#include "gps.h"

// Change this according to which UART you plan to use
static const uart_e gps_uart = UART__2;

// Space for the line buffer, and the line buffer data structure instance
static char line_buffer[200];
static line_buffer_s line;

static gps_coordinates_t parsed_coordinates;

// gpio_s gpio__construct_with_function(gpio__port_e port, uint8_t pin_number_0_to_31, gpio__function_e function) {
//   gpio_s gpio = gpio__construct(port, pin_number_0_to_31);
//   gpio__set_function(gpio, function);
//   return gpio;
// }

// // uart 123 initialization
// void uart123_init(void) {
//   // UART1 is on P0.15, P0.16
//   gpio__construct_with_function(GPIO__PORT_0, 15, GPIO__FUNCTION_1); // P0.15 - Uart-1 Tx
//   gpio__construct_with_function(GPIO__PORT_0, 16, GPIO__FUNCTION_1); // P0.16 - Uart-1 Rx

//   // UART2 is on P0.10, P0.11
//   gpio__construct_with_function(GPIO__PORT_0, 10, GPIO__FUNCTION_1); // P0.10 - Uart-2 Tx
//   gpio__construct_with_function(GPIO__PORT_0, 11, GPIO__FUNCTION_1); // P0.11 - Uart-2 RX

//   // UART3 is on P4.28, P4.29
//   gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2); // P4.28 - Uart-3 Tx
//   gpio__construct_with_function(GPIO__PORT_4, 29, GPIO__FUNCTION_2); // P4.29 - Uart-3 Rx
// }

// static void gps__transfer_data_from_uart_driver_to_line_buffer(void) {
void gps__transfer_data_from_uart_driver_to_line_buffer(void) {
  char byte;
  const uint32_t zero_timeout = 0;

  printf("get or not: %d\n", uart__get(gps_uart, &byte, zero_timeout));
  printf("byte: %c\n", byte);

  while (uart__get(gps_uart, &byte, zero_timeout)) {
    line_buffer__add_byte(&line, byte);
  }
}

// Parse latitude and longitude information from a NMEA 0183 formatted $GPGGA sentence
// sentence: NMEA 0183 formatted string
// coordinates: structure to store the parsed results
void parse_gpgga(const char *sentence, gps_coordinates_t *coordinates) {
  float lat_deg, lon_deg; // Latitude and longitude degrees
  char lat_dir, lon_dir;  // Latitude and longitude direction (N/S, E/W)

  // Use sscanf to parse the data from the string, following the NMEA 0183 standard format
  int ret = sscanf(sentence, "$GPGGA,%*f,%f,%c,%f,%c,%*f,%*d,%*f,%*f,M,%*f,M,%*f,%*d", &lat_deg, &lat_dir, &lon_deg,
                   &lon_dir);

  printf("ret: %d\n", ret);

  // Check if the parsing was successful
  if (ret == 4) {
    // Adjust the sign of latitude and longitude based on direction
    coordinates->latitude = (lat_dir == 'S' || lat_dir == 'N') ? -lat_deg : lat_deg;
    coordinates->longitude = (lon_dir == 'W' || lon_dir == 'E') ? -lon_deg : lon_deg;
  } else {
    // Parsing error, set to default values
    coordinates->latitude = 0.0;
    coordinates->longitude = 0.0;
  }
}

// static void gps__parse_coordinates_from_line(void) {
void gps__parse_coordinates_from_line(void) {
  char gps_line[200];
  printf("enter gps__parse_coordinates_from_line\n");
  // Before running the code on the arm machine, comment out the following two sentences
  parsed_coordinates.latitude = 48.1173;
  parsed_coordinates.longitude = 11.5167;
  if (line_buffer__remove_line(&line, gps_line, sizeof(gps_line))) {
    // TODO: Parse the line to store GPS coordinates etc.
    // TODO: parse and store to parsed_coordinates
    printf("gps_line: %s\n", gps_line);
    parse_gpgga(gps_line, &parsed_coordinates);
  }
}

void gps__init(void) {
  line_buffer__init(&line, line_buffer, sizeof(line_buffer));
  uart__init(gps_uart, clock__get_peripheral_clock_hz(), 38400);

  // UART1 is on P0.15, P0.16
  gpio__construct_with_function(GPIO__PORT_0, 15, GPIO__FUNCTION_1); // P0.15 - Uart-1 Tx
  gpio__construct_with_function(GPIO__PORT_0, 16, GPIO__FUNCTION_1); // P0.16 - Uart-1 Rx

  // UART2 is on P0.10, P0.11
  gpio__construct_with_function(GPIO__PORT_0, 10, GPIO__FUNCTION_1); // P0.10 - Uart-2 Tx
  gpio__construct_with_function(GPIO__PORT_0, 11, GPIO__FUNCTION_1); // P0.11 - Uart-2 RX

  // UART3 is on P4.28, P4.29
  gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2); // P4.28 - Uart-3 Tx
  gpio__construct_with_function(GPIO__PORT_4, 29, GPIO__FUNCTION_2); // P4.29 - Uart-3 Rx

  // RX queue should be sized such that can buffer data in UART driver until gps__run_once() is called
  // Note: Assuming 38400bps, we can get 4 chars per ms, and 40 chars per 10ms (100Hz)
  QueueHandle_t rxq_handle = xQueueCreate(50, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(8, sizeof(char)); // We don't send anything to the GPS
  uart__enable_queues(gps_uart, rxq_handle, txq_handle);
}

void gps__run_once(void) {
  gps__transfer_data_from_uart_driver_to_line_buffer();
  gps__parse_coordinates_from_line();
}

gps_coordinates_t gps__get_coordinates(void) {
  // TODO return parsed_coordinates
  // printf("latitude: %f\n", parsed_coordinates.latitude);
  // printf("longitude: %f\n", parsed_coordinates.longitude);
  return parsed_coordinates;
}