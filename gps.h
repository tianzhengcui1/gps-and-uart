// gps.h
#pragma once

#include "gpio.h"
#include <stdio.h>

// GPS module dependency
#include "line_buffer.h"
#include "uart.h"
// #include "uart3_init.h"

#include "clock.h" // needed for UART initialization

// Note:
// South means negative latittude
// West means negative longitutde
typedef struct {
  float latitude;
  float longitude;
} gps_coordinates_t;

void gps__init(void);
void gps__run_once(void);

gps_coordinates_t gps__get_coordinates(void);